####Build Application

`$ gradle clean build`

After running build task, the .jar file will be available at the following location:
> sealight-demo-app\build\libs

The application be executed from .jar file using the following command line:
`$ java -jar sealight-demo-app-0.0.1-SNAPSHOT.jar `

####Run Spring-boot 
`$ gradle bootRun`

####Application Endpoints:
 - endpoint: /products
 - response: 
 ```javascript
 [
     {
         "id": "75a5",
         "name": "Samsung TV 42",
         "price": 1500
     },
     {
         "id": "85b7",
         "name": "iPhone 8",
         "price": 2500
     },
    {
         "id": "66c8",
         "name": "MacBook Pro",
        "price": 4700
    }
 ]
 ```