package com.demo.sealightdemoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SealightDemoAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SealightDemoAppApplication.class, args);
	}

}
