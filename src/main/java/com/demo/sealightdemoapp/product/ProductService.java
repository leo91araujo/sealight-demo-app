package com.demo.sealightdemoapp.product;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final List<Product> productList;

    public ProductService() {
        this.productList = new LinkedList<>();
    }

    @PostConstruct
    public void init() {
        productList.add(new Product("75a5", "Samsung TV 42", new BigDecimal(1500.00)));
        productList.add(new Product("85b7", "iPhone 8", new BigDecimal(2500.00)));
        productList.add(new Product("66c8", "MacBook Pro", new BigDecimal(4700.00)));
    }

    public List<Product> getProductList() {
        return productList;
    }

    public Optional<Product> findById(String id) {
        return productList.stream()
                .filter(p -> id.equals(p.getId()))
                .findFirst();
    }

    public List<Product> findByName(String name) {
        return productList.stream()
                .filter(p -> p.getName().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }
}
