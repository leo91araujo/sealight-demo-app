package com.demo.sealightdemoapp.product;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/products")
public class ProductController {

    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> findProduct (@RequestParam(required = false) String name) {
        if (name != null) {
            return productService.findByName(name);
        }

        return productService.getProductList();
    }

    @GetMapping("/{id}")
    public Optional<Product> getById(@PathVariable final String id){
        return productService.findById(id);
    }
}
