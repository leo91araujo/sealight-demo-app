package com.demo.sealightdemoapp.product;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class ProductServiceTest {

    private ProductService productService;

    @Before
    public void setup() {
        this.productService = new ProductService();
        productService.init();
    }
    @Test
    public void whenFetProductListThenReturnsList() {
        List<Product> productList = productService.getProductList();
        assertEquals(3, productList.size());
    }

    @Test
    public void givenIdExistWhenFindByIdThenReturnsProduct() {
        Product product = productService.findById("66c8").get();

        assertNotNull(product);
        assertEquals("MacBook Pro", product.getName());
    }
//
//    @Test
//    public void givenNameExistsWhenFindByNameThenReturnsProduct() {
//        List<Product> products = productService.findByName("MacBook Pro");
//        assertEquals(1, products.size());
//    }
}
