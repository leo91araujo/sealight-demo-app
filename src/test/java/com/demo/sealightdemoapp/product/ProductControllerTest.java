package com.demo.sealightdemoapp.product;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ProductControllerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private ProductService productService;

    private ProductController productController;

    @Before
    public void setup() {
        this.productController = new ProductController(productService);
    }

    @Test
    public void givenNameIsPresentWhenFindProductThenReturnsProduct() {
        when(productService.findByName(any())).thenReturn(any());

        productController.findProduct("MacBook");

        verify(productService, times(1)).findByName("MacBook");
        verify(productService, times(0)).getProductList();
    }

    @Test
    public void givenNameIsNullWhenFindProductThenReturnsProduct() {
        productController.findProduct(null);

        verify(productService, times(0)).findByName("MacBook");
        verify(productService, times(1)).getProductList();
    }
}
